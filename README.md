# brian's systemd units

a set of systemd units (mostly for --user) i've written for my systemd/Linux machines - mostly Arch/Manjaro ones for packages lacking a proper service unit, but they have their uses elsewhere. mostly kept here for easy backup and cloning on other machines. you're more than welcome to use 'em if you need 'em.
